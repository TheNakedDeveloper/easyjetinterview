using System.Diagnostics;
using System.Linq;
using System;
using NUnit.Framework;
using Ninject;
using Ninject.Modules;

namespace Interview
{
    [TestFixture]
    public class Tests
    {
        IKernel kernal;

        [TestFixtureSetUp]
        public void Initialise()
        {
            kernal = new StandardKernel();
            kernal.Bind<IStoreable>().To<InMemoryStorage>();
            kernal.Bind<IRepository<IStoreable>>().To<InMemoryRepository<IStoreable>>();
        }

        [Test]
        public void InMemoryRepository_AddExpectIncreaseCount()
        {
            var repo = BuildRepository();  
            var expectedResult = repo.All().Count() + 1;
            repo.Save(CreateStorageItem());
            var actualResult = repo.All().Count();

            Assert.AreEqual(expectedResult, actualResult, "Item added to repository, count did not increase by 1.");
        }
        
        [Test]
        public void InMemoryRepository_UpdateExpectNoIncreasedCount()
        {
            var repo = BuildRepository();
            var storageItem = CreateStorageItem();

            repo.Save(storageItem);

            var expectedResult = repo.All().Count();
            repo.Save(storageItem);
            var actualResult = repo.All().Count();

            Assert.AreEqual(expectedResult, actualResult, "Item added, then re-saved, count changed on re-save.");
        }

        [Test]
        public void InMemoryRepository_DeleteExpectDecrementCount()
        {
            var repo = BuildRepository();
            var storageItem = CreateStorageItem();
            repo.Save(storageItem);

            var expectedResult = repo.All().Count() - 1;
            repo.Delete(storageItem.Id);
            var actualResult = repo.All().Count();

            Assert.AreEqual(expectedResult, actualResult, "Item added then removed, count did not decrement by 1.");
        }

        [Test]
        public void InMemoryRepository_FindKnownItemExpectFound()
        {
            var repo = BuildRepository();
            var storageItem = CreateStorageItem();

            repo.Save(storageItem);

            var expectedResult = true;
            var foundItem = repo.FindById(storageItem.Id);
            var actualResult = (foundItem != null && foundItem.Id == storageItem.Id);
            
            Assert.AreEqual(expectedResult, actualResult, "Didn't find an item known to be in the repository.");
        }

        [Test]
        public void InMemoryRepository_FindUnknownItemExpectNotFound()
        {
            var repo = BuildRepository();
            var storageItem = CreateStorageItem();
            
            var expectedResult = false;
            var foundItem = repo.FindById(storageItem.Id);
            var actualResult = (foundItem != null);

            Assert.AreEqual(expectedResult, actualResult, "Found an item that should not have been in the repository.");
        }

        [Test]
        public void InMemoryRepository_GetAllExpectNumberAdded()
        {
            var repo = BuildRepository();
            const Int32 expectedVolume = 16;

            for (Int32 i = 0; i < expectedVolume; i++)
            {
                repo.Save(CreateStorageItem());
            }

            Int32 expectedResult = expectedVolume;
            var allItems = repo.All();
            Int32 actualResult = allItems.Count();

            Assert.AreEqual(expectedResult, actualResult, expectedVolume.ToString() + " items added to repository but not all were available.");
        }

        private IStoreable CreateStorageItem()
        {
            Guid itemID = Guid.NewGuid();
            var item = kernal.Get<IStoreable>();
            item.Id = itemID;
            return item;
        }

        private IRepository<IStoreable> BuildRepository()
        {
            return kernal.Get<IRepository<IStoreable>>();
        }

    }
}