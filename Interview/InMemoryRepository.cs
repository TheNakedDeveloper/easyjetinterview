﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Interview
{
    public class InMemoryRepository<T> : IRepository<T>  where T : IStoreable
    {
        private List<T> _Storage;

        public InMemoryRepository()
        {
            _Storage = new List<T>();
        }

        public IEnumerable<T> All()
        {
            return _Storage;
        }

        public void Delete(IComparable id)
        {
            T found = FindById(id);
            if (!IsEmpty(found))
                _Storage.Remove(found);
        }

        public void Save(T item)
        {
            T found = FindById(item.Id);
            if (!IsEmpty(found))
                found = item;
            else
                _Storage.Add(item);
        }

        public T FindById(IComparable id)
        {
            var searchResult = _Storage.Where(x => x.Id.CompareTo(id) == 0);
            if (searchResult.Count() > 0)
                return searchResult.SingleOrDefault();
            else
                return default(T);
        }

        private Boolean IsEmpty(T check)
        {
            return EqualityComparer<T>.Default.Equals(check, default(T));
        }

        ~InMemoryRepository()
        {
            _Storage.Clear();
        }
    }
}
