﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Interview
{
    public class InMemoryStorage : IStoreable
    {
        protected IComparable _Id;
        public IComparable Id
        {
            get
            { return _Id; }
            set
            { _Id = value; }
        }
    }
}
